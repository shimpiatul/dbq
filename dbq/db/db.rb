require 'mysql2'

module Db
  # Mysql manager
  class MySQL
    attr_reader :conn

    def connect(conn_)
      puts "Connecting to MySql on host #{conn_.host}"
      conn = {
          host: conn_.host,
          username: conn_.user,
          password: conn_.pwd,
          database: conn_.dbname
      }

      @conn = Mysql2::Client.new(conn)
    end

    def execute(cmd)
      puts "Executing #{cmd} on MySql"
      @conn.query(cmd)
    end
  end

  # PostgreSql manager
  class PostgreSql
    def connect(conn)
      puts 'Support for PostgreSql will be added in next release'
    end

    def execute(cmd)

    end
  end

  # MS Sql manager
  class PostgreSql
    def connect(conn)
      puts 'Support for MS Sql will be added in next release'
    end

    def execute(cmd)

    end
  end

  # Oracle manager
  class PostgreSql
    def connect(conn)
      puts 'Support for Oracle will be added in next release'
    end

    def execute(cmd)

    end
  end

  # supported databases
  PROVIDERS = [:mysql, :pg, :mssql, :oracle, :mongodb]

  # returns db connection
  def self.conn(conn)
    providers_map = {
        mysql: MySQL.new,
        pg: PostgreSql.new
    }

    providers_map[conn.provider.to_sym].connect(conn)

    providers_map[conn.provider.to_sym]
  end

end