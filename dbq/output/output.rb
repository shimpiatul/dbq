require 'json'

module  Output
  # base class
  class Base

    def write(filename, data)
      if filename == 'console'
        yield $stdout if block_given?
        $stdout.puts
      else
        File.open(filename, "w") do |file|
          yield file if block_given?
          file.close
        end
      end
    end

  end

  # default (unformatted)
  class NoFormat < Base
    def write(file, data)
      super do |file|
        data.each { |data_| file.puts(data_) }
      end
    end
  end

  # csv
  class CSV < Base

    def write(file, data)
      super do |file|
        data.each { |data_| file.puts(data_.values.join(',')) }
      end
    end

  end

  # json
  class JSON < Base
    def write(file, data)
      super do |file|
        file.write data.take(data.size).to_json
      end
    end
  end

  # pdf
  class PDF < Base
    def write(file, data)
      super do |file|
        file.write 'Support for PDF will be available in next release'
      end
    end
  end

  # xml
  class XML < Base
    def write(file, data)
      super do |file|
        file.write 'Support for XML will be available in next release'
      end
    end
  end

  # supported output formats
  module FORMAT
    CSV = 'csv'
    JSON = 'json'
    PDF = 'pdf'
    XML = 'xml'
  end
  
  def self.write(output, file, data)
    fileHandlerMap = {
       default: NoFormat.new,
       csv: CSV.new,
       json: JSON.new,
       pdf: PDF.new,
       xml: XML.new
    }

    fileHandlerMap[output.to_sym].write(file, data)
  end

end