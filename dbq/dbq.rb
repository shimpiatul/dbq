require_relative './argument/argument'
require_relative './db/db'
require_relative './output/output'

module  Dbq
  def self.args_valid?(args)
    Argument::args_valid?(args)
  end

  def self.normalize_args(args)
    Argument::normalize(args)
  end

  def self.write(format, file, data)
    Output::write(format, file, data)
  end

  def self.execute_db_cmd(cmd)
    conn = Db::conn(cmd)

    conn.execute(cmd.query) if !conn.nil?
  end
end