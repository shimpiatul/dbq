module Argument
  VALID_ARGS = [:provider, :host, :user, :pwd, :dbname, :query, :output, :file, :console]
  MANDATORY_ARGS_FOR_RUN_MODE = [:host, :user, :pwd, :dbname, :query]
  MANDATORY_ARGS_FOR_CONSOLE_MODE = [:host, :user, :pwd, :dbname]


  # arguments class - this will hold the passed arguments as well as default arguments
  class Arguments
    args = VALID_ARGS + [:console]
    args.each { |valid_arg_| attr_reader valid_arg_.to_sym }

    def initialize(args)
      @provider = (args[:provider].nil? || args[:provider].empty?) ? 'mysql' : args[:provider]
      @host = args[:host]
      @user = args[:user]
      @pwd = args[:pwd]
      @dbname = args[:dbname]
      @query = args[:query]
      @output = (args[:output].nil? || args[:output].empty?) ? 'default' : args[:output]
      @file = (args[:file].nil? || args[:file].empty?) ? "console" : args[:file]
      @file = (args[:file].nil? || args[:file].empty?) ? "console" : args[:file]
      @console = !args[:console].nil?
    end

  end

  def self.args_valid?(args_)
    args = parse_arg(args_)

    # check if all arguments needed for RUN mode are passed or not
    if(args[:console].nil?)
      missing_args = MANDATORY_ARGS_FOR_RUN_MODE.select { |arg_| arg_ if args[arg_].nil? }
      if(missing_args.length > 0)
        raise "Please specify #{missing_args.join(',')} argument(s)"
      else
        return missing_args.length == 0 ? true : false
      end
    else
      true
    end
  end

  def self.parse_arg(args)
    args_join = args.join.split('--')
    args_map = { }

    args_join.each do |arg_|
      VALID_ARGS.each do |valid_arg_|
        pos = arg_.index(valid_arg_.to_s)
        if !pos.nil? && pos == 0
          args_map[valid_arg_] = arg_[valid_arg_.length..-1]
        end
      end
    end

    # assign default values to optional arguments
    args_map[:host] =  args_map[:host] || 'localhost'
    args_map
  end

  def self.normalize(args)
    Arguments.new(parse_arg(args))
  end
end