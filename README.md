# Welcome to dbq

## What's dbq ?

dbq is a remote database querying and management command line utility for any database.

Currently is supports below databases

* MySql
* PostreSql
* Microsoft Sql Server
* Oracle
* MongoDb

# Design
## Design Principles/Best practises Followed
* Open Close Principle  
  Below functionality can be added quickly, by adding new code instead of changing existing code  
    * Support for new databases    
    * New arguments
    * Arguments validations
* Single Responsibility Principle  
  Every module, class or function does only single task/logic.
* Domain Driven Design
  Problem is broken into domain objects and scope and then responsibilites of each module or object is implemented
* Layered architecture
* Convention over configuration
  Default values or configuration assumed.

## Class Diagram
![Alt text](/hld.png)

## Modules
### App module
The _**App module**_ is main module which receives user input and calls api's in other modules
(Argument, Database and Output) to query the database.

### Argument module
####Does below tasks  
* Check if all required arguments are passed or not
* Arguments values are valid
* Assign default values to optional arguments

### Database module

This module implements database connection and querying logic.
It implements drivers for all supported databases. The drivers make calls to native database libraries on respective
platform.
### Output module
####This module manages the output of database query.It performs below tasks
* Format the output in csv, json, xml or pdf (as and if specified by user)
* Show the formatted on console or save it for disk file

# Improvements pending
* Error handling
* Allowing user to manage database in console like MySql
* Arguments Validations
* Write Unit and e2e tests 

## Getting Started
### Debian/Ubuntu
1. Install ruby (ruby ver 2.5.1p57 is recommended). Also using rvm to install ruby is recommended.

        $ sudo apt-get install ruby-full
        
2. Clone/download the repository.

        $ git clone https://shimpiatul@bitbucket.org/shimpiatul/dbq.git
        
3. Go inside the repository and give below command

        $ bundle install

### Querying MySql Database
        
1. Install MySql native libs.

        $ sudo apt-get install libmysqlclient-dev

2. Querying database.  
View data from table mailbox inside database mailserver on host mail-server.rds.amazonaws.com.

        $ ruby dbq.rb --provider mysql --host mail-server.rds.amazonaws.com --user support --pwd support@1234 --dbname mailserver --query "select * from mailbox"

    View data in json format. Add --output json.

        $ ruby dbq.rb --provider mysql --host mail-server.rds.amazonaws.com --user support --pwd support@1234 --dbname mailserver --query "select * from mailbox" --output json
    Add pdf and xml to view data in pdf and xml format.

    Save data in file Add --file flag with filename

        $ ruby dbq.rb --provider mysql --host mail-server.rds.amazonaws.com --user support --pwd support@1234 --dbname mailserver --query "select * from mailbox" --output json --file data.json
 
For querying other database first install their native libraries and then change the provider flag.
ex to query Postgresql pass --provider pg
   

## Code Status

[![Build Status](https://travis-ci.org/rails/rails.svg?branch=master)](https://travis-ci.org/rails/rails)

## License

dbq is free available as it is