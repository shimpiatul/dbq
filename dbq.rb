require_relative './dbq/dbq.rb'

begin
  # 1st - validate command line arguments
  if Dbq::args_valid?(ARGV)
    # 2nd - normalize arguments
    normalized_args = Dbq::normalize_args(ARGV)

    puts "Normalized args : #{normalized_args.inspect}"
    # 3rd - execute command against db and write result to output
    Output::write(normalized_args.output, normalized_args.file, Dbq::execute_db_cmd(normalized_args))
  else
    puts 'Arguments invalid'
  end
rescue Exception => e
  puts "#{e.message}"
ensure
  puts 'Good bye...have a nice day'
end
